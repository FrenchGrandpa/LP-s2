﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Casus_Containervervoer;
using System.Collections.Generic;

namespace ContainervervoerTests {

    [TestClass]
    public class ContainervervoerTest {

        private static List<Container> generateRandomContainers(int amt) {
            Random rnd = new Random();
            List<Container> containers = new List<Container>();
            for (int i = 0; i < amt; i++) {
                containers.Add(new Container(rnd.Next(0, Container.maxGewicht + 1), (ContainerType) rnd.Next(0, 3)));
            }
            return containers;
        }

        [TestMethod]
        public void bootThrowedExceptionAlsBreedteOnevenIs() {
            bool errorThrown = false;
            try {
                new Boot(3000, new Afmeting(6, 11));
            }
            catch (Exception) {
                errorThrown = true;
            }
            Assert.IsTrue(errorThrown);
        }

        [TestMethod]
        public void maxGewichtOpCointainerIsNietHogerDan120() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(3000, new Afmeting(6, 10));
            boot.populate(containers);
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y; y++) {
                    Assert.IsTrue(boot.matrix[x, y].gewichtZonderEerste <= Container.maxGewichtBovenop);
                }
            }
        }

        [TestMethod]
        public void containerThrowedExceptionAlsGewichtNegatiefIs() {
            bool errorThrown = false;
            try {
                new Container(-1, ContainerType.GEKOELD);
            }
            catch (IndexOutOfRangeException) {
                errorThrown = true;
            }
            Assert.IsTrue(errorThrown);
        }

        [TestMethod]
        public void containerThrowedExceptionAlsGewichtTeGrootIs() {
            bool errorThrown = false;
            try {
                new Container(123, ContainerType.GEKOELD);
            }
            catch (IndexOutOfRangeException) {
                errorThrown = true;
            }
            Assert.IsTrue(errorThrown);
        }

        [TestMethod]
        public void erZijnGeenContainersBovenopWaardevolleContainers() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(3000, new Afmeting(6, 10));
            boot.populate(containers);
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y; y++) {
                    bool waardevolleContainerAanwezig = false;
                    foreach (Container container in boot.matrix[x, y]) {
                        Assert.IsFalse(waardevolleContainerAanwezig && container.type == ContainerType.WAARDEVOL);
                        if (container.type == ContainerType.WAARDEVOL)
                            waardevolleContainerAanwezig = true;
                    }
                }
            }
        }

        [TestMethod]
        public void waardevolleContainersZijnAltijdVoorOfAchteraan() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(3000, new Afmeting(6, 10));
            boot.populate(containers);
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y; y++) {
                    foreach (Container container in boot.matrix[x, y]) {
                        Assert.IsFalse(container.type == ContainerType.WAARDEVOL && x > 0 && x < boot.afmeting.x - 1);
                    }
                }
            }
        }

        [TestMethod]
        public void gekoeldeContainersZijnAltijdVoorOfAchteraan() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(3000, new Afmeting(6, 10));
            boot.populate(containers);
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y; y++) {
                    foreach (Container container in boot.matrix[x, y]) {
                        Assert.IsFalse(container.type == ContainerType.GEKOELD && x > 0 && x < boot.afmeting.x - 1);
                    }
                }
            }
        }

        [TestMethod]
        public void verschilInGewichtTussenLinksEnRechtsWijktNietMeerAfDan20Procent() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(3000, new Afmeting(6, 10));
            boot.populate(containers);
            double gewichtLinks = 0;
            double gewichtRechts = 0;
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y / 2; y++) {
                    gewichtLinks += boot.matrix[x, y].gewicht;
                    gewichtRechts += boot.matrix[x, y + boot.afmeting.y / 2].gewicht;
                }
            }
            Assert.IsTrue(gewichtLinks >= gewichtRechts * 0.8 && gewichtRechts >= gewichtLinks * 0.8);
            Assert.IsTrue(gewichtLinks <= gewichtRechts * 1.2 && gewichtRechts <= gewichtLinks * 1.2);
        }

        [TestMethod]
        public void helftVanCapaciteitWordtGebruikt() {
            var containers = generateRandomContainers(10000);
            Boot boot = new Boot(10000, new Afmeting(6, 10));
            boot.populate(containers);
            double gewicht = 0;
            for (int x = 0; x < boot.afmeting.x; x++) {
                for (int y = 0; y < boot.afmeting.y; y++) {
                    gewicht += boot.matrix[x, y].gewicht;
                }
            }
            Assert.IsTrue(gewicht >= boot.capaciteit / 2);
        }

    }

}
