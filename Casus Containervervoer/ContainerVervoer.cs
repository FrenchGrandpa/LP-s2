﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casus_Containervervoer {

    public class ContainerVervoer {

        private static Boot boot = new Boot(3000, new Afmeting(10, 6));

        static void Main(string[] args) {
            var containers = generateRandomContainers(10000);
            boot.populate(containers);
            Console.WriteLine(JsonConvert.SerializeObject(boot.matrix, Formatting.Indented));
            Console.ReadLine();
        }


        private static List<Container> generateRandomContainers(int amt) {
            Random rnd = new Random();
            List<Container> containers = new List<Container>();
            for (int i = 0; i < amt; i++) {
                containers.Add(new Container(rnd.Next(0, Container.maxGewicht + 1), (ContainerType) rnd.Next(0, 3)));
            }
            return containers;
        }
    }

    public class Boot {

        private List<Container> voorsteRij = new List<Container>();
        private List<Container> achtersteRij = new List<Container>();
        private List<List<Container>> linkerKant = new List<List<Container>>();
        private List<List<Container>> rechterKant = new List<List<Container>>();

        public Stapel[,] matrix;

        public int capaciteit;
        public const double minCapaciteitBenutRatio = 0.5;
        public const double linksRechtsMaxVerschilRatio = 0.2;
        public Afmeting afmeting;

        public Boot(int capaciteit, Afmeting afmeting) {
            if (afmeting.y % 2 != 0)
                throw new Exception();
            this.capaciteit = capaciteit;
            this.afmeting = afmeting;
            matrix = new Stapel[afmeting.x, afmeting.y];
            for (int x = 0; x < afmeting.x; x++) {
                for (int y = 0; y < afmeting.y; y++) {
                    matrix[x, y] = new Stapel();
                }
            }
        }

        public void populate(List<Container> containers) {
            //Sorteer containers per type
            List<Container> normaal = new List<Container>();
            List<Container> waardevol = new List<Container>();
            List<Container> gekoeld = new List<Container>();

            foreach (Container container in containers) {
                switch (container.type) {
                    case ContainerType.NORMAAL:
                        normaal.Add(container);
                        break;
                    case ContainerType.WAARDEVOL:
                        waardevol.Add(container);
                        break;
                    case ContainerType.GEKOELD:
                        gekoeld.Add(container);
                        break;
                }
            }

            //Sorteer containers per gewicht, aflopend
            normaal = normaal.OrderByDescending(i => i.Gewicht).ToList();
            waardevol = waardevol.OrderByDescending(i => i.Gewicht).ToList();
            gekoeld = gekoeld.OrderByDescending(i => i.Gewicht).ToList();

            plaatsNormaleContainers(normaal);
            plaatsGekoeldeContainers(gekoeld);
            gekoeld.AddRange(plaatsWaardevolleContainers(waardevol));
        }

        private void plaatsNormaleContainers(List<Container> normaal) {
            int minGewicht = normaal[0].Gewicht;
            while (normaal.Count > 0 && minGewicht + normaal[0].Gewicht <= Container.maxGewichtBovenop) {
                for (int x = 1; x < afmeting.x - 1; x++) {//Skipt eerste en laaste rij
                    for (int y = 0; y < afmeting.y / 2; y++) {
                        if (normaal.Count == 0)
                            return;
                        if (matrix[x, y].gewichtZonderEerste + normaal[0].Gewicht > Container.maxGewichtBovenop)
                            continue;

                        matrix[x, y].Add(normaal[0]);
                        minGewicht = Math.Min(minGewicht + normaal[0].Gewicht, matrix[x, y].gewichtZonderEerste);
                        normaal.RemoveAt(0);

                        if (normaal.Count == 0)
                            return;
                        if (matrix[x, y + afmeting.y / 2].gewichtZonderEerste + normaal[0].Gewicht > Container.maxGewichtBovenop)
                            continue;

                        matrix[x, y + afmeting.y / 2].Add(normaal[0]);
                        minGewicht = Math.Min(minGewicht + normaal[0].Gewicht, matrix[x, y].gewichtZonderEerste);
                        normaal.RemoveAt(0);
                    }
                }
            }

            //Console.WriteLine(normaal.Count);
            //Console.WriteLine(minGewicht);
            //Console.ReadLine();
        }

        private void plaatsGekoeldeContainers(List<Container> gekoeld) {
            int minGewicht = gekoeld[0].Gewicht;
            while (gekoeld.Count > 0 && minGewicht + gekoeld[0].Gewicht <= Container.maxGewichtBovenop) {
                plaatsGekoeldeContainersOp(gekoeld, ref minGewicht, 0);
                plaatsGekoeldeContainersOp(gekoeld, ref minGewicht, afmeting.x - 1);
            }
        }

        private void plaatsGekoeldeContainersOp(List<Container> gekoeld, ref int minGewicht, int x) {//List<Container> gekoeld hoeft geen ref keyword te hebbem, want die word sws al als reference doorgegeven
            for (int y = 0; y < afmeting.y / 2; y++) {
                if (gekoeld.Count == 0)
                    return;
                if (matrix[x, y].gewichtZonderEerste + gekoeld[0].Gewicht > Container.maxGewichtBovenop)
                    continue;

                matrix[x, y].Add(gekoeld[0]);
                minGewicht = Math.Min(minGewicht + gekoeld[0].Gewicht, matrix[x, y].gewichtZonderEerste);
                gekoeld.RemoveAt(0);

                if (gekoeld.Count == 0)
                    return;
                if (matrix[x, y + afmeting.y / 2].gewichtZonderEerste + gekoeld[0].Gewicht > Container.maxGewichtBovenop)
                    continue;

                matrix[x, y + afmeting.y / 2].Add(gekoeld[0]);
                minGewicht = Math.Min(minGewicht + gekoeld[0].Gewicht, matrix[x, y].gewichtZonderEerste);
                gekoeld.RemoveAt(0);
            }
        }

        private List<Container> plaatsWaardevolleContainers(List<Container> waardevol) {
            List<Container> verwijderdeGekoeldeContainers = new List<Container>();
            plaatsWaardevolleContainersOp(waardevol, verwijderdeGekoeldeContainers, 0);
            plaatsWaardevolleContainersOp(waardevol, verwijderdeGekoeldeContainers, afmeting.x - 1);
            return verwijderdeGekoeldeContainers;
        }

        private void plaatsWaardevolleContainersOp(List<Container> waardevol, List<Container> verwijderdeGekoeldeContainers, int x) {
            for (int y = 0; y < afmeting.y; y++) {
                if (matrix[x, y].gewichtZonderEerste < Container.maxGewichtBovenop) {
                    int verschil = Math.Min(Container.maxGewichtBovenop - matrix[x, y].gewichtZonderEerste, Container.maxGewicht);
                    int i = 0;
                    for (; i < waardevol.Count; i++) {
                        if (waardevol[i].Gewicht == verschil) {
                            matrix[x, y].Add(waardevol[i]);
                            waardevol.RemoveAt(i);
                            break;
                        }
                    }
                }
                else {
                    int verschil = matrix[x, y][matrix[x, y].Count - 1].Gewicht;
                    int i = 0;
                    for (; i < waardevol.Count; i++) {
                        if (waardevol[i].Gewicht == verschil) {
                            verwijderdeGekoeldeContainers.Add(matrix[x, y][matrix[x, y].Count - 1]);
                            matrix[x, y][matrix[x, y].Count - 1] = waardevol[i];
                            waardevol.RemoveAt(i);
                            break;
                        }
                    }
                }
            }
        }
    }

    public class Stapel : List<Container> {

        public int gewicht = 0;
        public int gewichtZonderEerste = 0;

        public new void Add(Container container) {
            base.Add(container);
            gewicht += container.Gewicht;
            if (this.Count > 1)
                gewichtZonderEerste += container.Gewicht;
        }

    }

    public class Container {

        public const int leegGewicht = 4;
        public const int maxGewicht = 26;
        public const int maxGewichtBovenop = 120;
        private int gewicht;
        public ContainerType type;

        public int Gewicht {
            get {
                return gewicht + leegGewicht;
            }
        }

        public Container(int gewicht, ContainerType type) {
            if (gewicht < 0 || gewicht > maxGewicht)
                throw new IndexOutOfRangeException();
            this.gewicht = gewicht;
            this.type = type;
        }

    }

    public enum ContainerType {
        NORMAAL,
        WAARDEVOL,
        GEKOELD
    }

    public class Afmeting {

        public int x;
        public int y;

        public Afmeting(int x, int y) {
            this.x = x;
            this.y = y;
        }

    }

}
