﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;
using Serilog.Formatting.Json;

namespace Algoritme_Casus_Circustrein {

    class Program {


        private static ILogger log2 = new LoggerConfiguration()
            .WriteTo.Console(outputTemplate: "{Message}")
            .CreateLogger();
        static void Main(string[] args) {
            /*Dier[][] trein = new Dier[50][];
            for (int i = 0; i < trein.Length; i++) {
                trein[i] = new Dier[10];
                for (int j = 0; j < trein[i].Length; j++) {
                    trein[i][j] = new Dier();
                }
            }
            int treinIndex = 0;
            List<Dier> dieren = new List<Dier>();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++) {
                dieren.Add(new Dier(rnd.Next(0, 2) == 1, toFormaat(rnd.Next(0, 3))));
            }

            List<Dier> formaat1f = new List<Dier>();
            List<Dier> formaat1t = new List<Dier>();
            List<Dier> formaat3f = new List<Dier>();
            List<Dier> formaat3t = new List<Dier>();
            List<Dier> formaat5t = new List<Dier>();

            for (int i = 0; i < dieren.Count; i++) {
                if (dieren[i] == null)
                    continue;
                if (!dieren[i].isVegitarier && dieren[i].formaat == 5) {
                    trein[treinIndex][0] = dieren[i];
                    treinIndex++;
                    dieren[i] = null;
                }
                else if (dieren[i].isVegitarier) {
                    switch (dieren[i].formaat) {
                        case 1:
                            formaat1t.Add(dieren[i]);
                            break;
                        case 3:
                            formaat3t.Add(dieren[i]);
                            break;
                        case 5:
                            formaat5t.Add(dieren[i]);
                            break;
                    }
                }
                else {
                    switch (dieren[i].formaat) {
                        case 1:
                            formaat1f.Add(dieren[i]);
                            break;
                        case 3:
                            formaat3f.Add(dieren[i]);
                            break;
                    }
                }
            }

            while (formaat5t.Count > 0) {
                trein[treinIndex][0] = formaat5t[0];
                formaat5t.RemoveAt(0);
                if (formaat3f.Count > 0) {
                    trein[treinIndex][1] = formaat3f[0];
                    formaat3f.RemoveAt(0);
                }
                else if (formaat3f.Count > 0) {
                    trein[treinIndex][1] = formaat3f[0];
                    formaat3f.RemoveAt(0);
                }
                else if (formaat3t.Count > 0) {
                    trein[treinIndex][1] = formaat3t[0];
                    formaat3t.RemoveAt(0);
                    if (formaat1f.Count > 0) {
                        trein[treinIndex][2] = formaat1f[0];
                        formaat1f.RemoveAt(0);
                    }
                    else {
                        for (int i = 2; i <= 3; i++) {
                            if (formaat1t.Count > 0) {
                                trein[treinIndex][i] = formaat1t[0];
                                formaat1t.RemoveAt(0);
                            }
                        }
                    }
                }
                treinIndex++;
            }

            while (formaat3f.Count > 0) {
                trein[treinIndex][0] = formaat3f[0];
                formaat3f.RemoveAt(0);
            }


            log(trein);
            Console.WriteLine("1f " + formaat1f.Count);
            Console.WriteLine("1t " + formaat1t.Count);
            Console.WriteLine("3f " + formaat3f.Count);
            Console.WriteLine("3t " + formaat3t.Count);
            Console.WriteLine("5t " + formaat5t.Count);
            Console.ReadLine();
            */
            /*double sum = 0;
            for (int i = 0; i < 100; i++) {
                sum += l();
            }*/
            //Console.WriteLine(sum / 100);
            l();
            Console.ReadLine();
        }

        public static int l() {
            List<List<Dier>> trein = new List<List<Dier>>();
            for (int i = 0; i < 100; i++) {
                trein.Add(new List<Dier>());//Init de wagonnen
            }
            List<Dier> dieren = new List<Dier>();
            Random rnd = new Random();
            for (int i = 0; i < 100; i++) {
                //dieren.Add(new Dier(rnd.Next(0, 2) == 1, toFormaat(rnd.Next(0, 3))));//Maak list met random elements
            }

            dieren.AddRange(new Dier[] {
                new Dier(true, 3),
                new Dier(true, 5),
                new Dier(true, 3),
                new Dier(true, 5),
                new Dier(true, 3)
            });

            List<Dier> formaat1f = new List<Dier>();
            List<Dier> formaat1t = new List<Dier>();
            List<Dier> formaat3f = new List<Dier>();
            List<Dier> formaat3t = new List<Dier>();
            List<Dier> formaat5f = new List<Dier>();
            List<Dier> formaat5t = new List<Dier>();

            foreach (Dier d in dieren) {//Sorteer per formaat en dieet
                if (d.isVegitarier) {
                    switch (d.formaat) {
                        case 1:
                            formaat1t.Add(d);
                            break;
                        case 3:
                            formaat3t.Add(d);
                            break;
                        case 5:
                            formaat5t.Add(d);
                            break;
                    }
                }
                else {
                    switch (d.formaat) {
                        case 1:
                            formaat1f.Add(d);
                            break;
                        case 3:
                            formaat3f.Add(d);
                            break;
                        case 5:
                            formaat5f.Add(d);
                            break;
                    }
                }
            }

            int index = 0;//Add elke soort erbij
            while (formaat5f.Count > 0) {
                trein[index].Add(formaat5f[0]);
                formaat5f.RemoveAt(0);
                index++;
            }
            int start = index;
            while (formaat5t.Count > 0) {
                trein[index].Add(formaat5t[0]);
                formaat5t.RemoveAt(0);
                if (getWeight(trein[index]) > 5)
                    index++;
            }
            index = start;
            while (formaat3f.Count > 0) {
                trein[index].Add(formaat3f[0]);
                formaat3f.RemoveAt(0);
                index++;
            }
            start = index;
            while (formaat3t.Count > 0) {
                trein[index].Add(formaat3t[0]);
                formaat3t.RemoveAt(0);
                if (getWeight(trein[index]) > 7)
                    index++;
            }
            index = start;
            while (formaat1f.Count > 0) {
                trein[index].Add(formaat1f[0]);
                formaat1f.RemoveAt(0);
                index++;
            }
            while (formaat1t.Count > 0) {
                trein[index].Add(formaat1t[0]);
                formaat1t.RemoveAt(0);
                if (getWeight(trein[index]) == 10)
                    index++;
            }

            for (int i = trein.Count - 1; i > 0; i--) {
                if (trein[i].Count == 0)
                    trein.RemoveAt(i);
            }

            //log(trein);
            Console.WriteLine(trein.Count);
            Console.WriteLine("1f " + formaat1f.Count);
            Console.WriteLine("1t " + formaat1t.Count);
            Console.WriteLine("3f " + formaat3f.Count);
            Console.WriteLine("3t " + formaat3t.Count);
            Console.WriteLine("5f " + formaat5f.Count);
            Console.WriteLine("5t " + formaat5t.Count);

            List<List<Dier>> notFull = new List<List<Dier>>();
            foreach (List<Dier> wagon in trein) {
                if (getWeight(wagon) < 10 && wagon[wagon.Count - 1].isVegitarier) {
                    notFull.Add(wagon);
                }
            }

            //log(notFull);
            //log2.Information("{trein}", trein);
            //log(new string[][] { new[] { "lol", "kaas", "is", "Lekker" }, new[] { "lol", "kaas", "is", "Lekker" }});
            Console.WriteLine(JsonConvert.SerializeObject(trein, Formatting.Indented));
            return trein.Count;
            //Console.ReadLine();
        }

        private static void log<T>(T obj) {
            if (obj is string) {
                Console.Write(obj);
                return;
            }

            if (obj is IList) {
                Console.WriteLine("[");
                bool firstLoop = true;
                foreach (var sub in (IList)obj) {
                    if (!firstLoop) {
                        Console.WriteLine(",");
                    }
                    firstLoop = false;
                    log(sub);
                }
                Console.Write("\n]");
            }
        }

        public static int getWeight(List<Dier> wagon) {
            int weight = 0;
            foreach (Dier d in wagon)
                weight += d.formaat;
            return weight;
        }

        public static int toFormaat(int i) {//Zet getal van 0-2 om naar 1, 3 of 5
            switch (i) {
                case 0: return 1;
                case 1: return 3;
                case 2: return 5;
            }
            return 0;
        }

        /*public static void log(Dier[][] dieren) {
            string str = "[";
            for (int i = 0; i < dieren.Length; i++) {
                str += "[";
                for (int j = 0; j < dieren[i].Length; j++) {
                    str += dieren[i][j].ToString();
                    if (j < dieren[i].Length - 1)
                        str += ", ";
                }
                str += "],\n";
            }
            str += "]";
            Console.WriteLine(str);
        }

        public static void log(List<List<Dier>> list) {
            string str = "[";
            for (int i = 0; i < list.Count; i++) {
                str += "[";
                for (int j = 0; j < list[i].Count; j++) {
                    str += list[i][j].ToString();
                    if (j < list[i].Count - 1)
                        str += ", ";
                }
                str += "],\n";
            }
            str += "]";
            Console.WriteLine(str);
        }*/
    }

    public class Dier {

        public bool isVegitarier;
        private int k;
        public int formaat { get { return k; } set { k = value; } }
        private bool empty = false;

        public Dier() {
            empty = true;
        }

        public Dier(bool isVegitarier, int formaat) {
            this.isVegitarier = isVegitarier;
            this.formaat = formaat;
        }

        /*public override string ToString() {
            return (empty) ? "{}" : "{ veg: " + isVegitarier.ToString() + ", form: " + formaat.ToString() + " }";
        }*/

    }
}
