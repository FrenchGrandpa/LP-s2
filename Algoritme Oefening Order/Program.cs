﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritme_Oefening_Order {

    class Program {

        static void Main(string[] args) {
            Product[] products = new Product[100];
            Random rnd = new Random();
            for (int i = 0; i < 100; i++) {
                products[i] = new Product("Item_" + rnd.Next(256).ToString(), rnd.NextDouble() * 100);
            }
            log(products);
            log(new Order(products).sort());
            Console.ReadLine();
        }

        public static void log(Product[] products) {
            string str = "[";
            for (int i = 0; i < products.Length; i++) {
                str += products[i].price.ToString("0.0", System.Globalization.CultureInfo.InvariantCulture);
                if (i < products.Length - 1)
                    str += ", ";
            }
            str += "]";
            Console.WriteLine(str);
        }
    }

    public class Product {

        public string name;
        public double price;

        public Product(string name, double price) {
            this.name = name;
            this.price = price;
        }
    }

    public class Order {

        private Product[] products;

        public Order(Product[] products) {
            this.products = products;
        }

        public double giveMaxPrice() {
            double max = 0;
            foreach (Product product in products) {
                if (product.price > max)
                    max = product.price;
            }
            return max;
        }

        public double giveMinPrice() {
            double min = double.MaxValue;
            foreach (Product product in products) {
                if (product.price < min)
                    min = product.price;
            }
            return min;
        }

        public Product[] getAllProducts(double minPrice) {
            List<Product> results = new List<Product>();
            foreach (Product product in products) {
                if (product.price >= minPrice)
                    results.Add(product);
            }
            return results.ToArray();
        }

        public Product[] sort() {
            for (int l = 0; l < 100; l++) {
                for (int i = 1; i < 100; i++) {
                    Product a = products[i - 1];
                    Product b = products[i];
                    if (b.price < a.price) {
                        products[i - 1] = b;
                        products[i] = a;
                    }
                }
            }
            return products;
        }

    }

}
 